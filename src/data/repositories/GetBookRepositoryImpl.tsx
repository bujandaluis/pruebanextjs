import { AxiosError } from "axios";
import { ApiNextJS } from "../sources/remote/api/ApiNextJS";
import { ResponseNextjs } from "../sources/remote/models/ResponseNextjs";
import { GetBookRepository } from "@/domain/repositories/GetBookRepository";

export class GetBookRepositoryImpl implements GetBookRepository {

    async getList(value?:string): Promise<ResponseNextjs> {
        try {
            if(value === undefined){
                value ='angular';
            }
            const response = await ApiNextJS.get<ResponseNextjs>('/v1/search_by_date?query=' + value + '&page=0');
            //console.log('RESPONSE 2: ' + JSON.stringify(response.data.hits[0].story_title));
            console.log('RESPONSE REPOSITORY: ' + JSON.stringify(response));
            return Promise.resolve(response.data)

        } catch (error) {

            let e = (error as AxiosError);
            console.log('ERROR: ' + JSON.stringify(e.response?.data));
            const apiError:ResponseNextjs = JSON.parse(JSON.stringify(e.response?.data));
            return Promise.resolve(apiError)
            
        }
    }

}
