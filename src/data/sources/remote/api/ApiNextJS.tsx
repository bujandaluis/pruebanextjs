import axios from 'axios';
const ApiNextJS = axios.create({
    baseURL: 'https://hn.algolia.com/api',
    headers: {
        'Content-Type': 'application/json'
    }
});

export { ApiNextJS } 