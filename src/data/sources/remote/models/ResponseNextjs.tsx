export interface ResponseNextjs {
    hits: Details[];
}


export interface Details {
    id: number;
    author: string;
    story_title: string;
    story_url: string;
    created_at: string;
    
}