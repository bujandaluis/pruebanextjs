import { ResponseNextjs } from "@/data/sources/remote/models/ResponseNextjs";

export interface GetBookRepository {

    getList(value?:string): Promise<ResponseNextjs>
    
}