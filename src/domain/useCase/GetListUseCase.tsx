import { GetBookRepositoryImpl } from "@/data/repositories/GetBookRepositoryImpl";

const { getList } = new GetBookRepositoryImpl();

export const GetListUseCase = async (value?:string) => {
    return await getList(value)
}