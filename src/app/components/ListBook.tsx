
import { Details } from '@/data/sources/remote/models/ResponseNextjs'
import moment from 'moment'
import React from 'react'

interface Props{
  lists: Details[]
}



export const ListBook = ({
  lists
}: Props) => {

  return (
   


    <div className="overflow-x-auto">
  <table className="table">
    {/* head */}
    <thead>
      <tr>
        <th>Autor</th>
        <th>Title</th>
       
        <th></th>
      </tr>
    </thead>
    <tbody>
      {
      
      lists.map(task => (
        <tr key={task.id}>
          <td>
            <div className="flex items-center gap-3">
              
              <div>
                <div className="font-bold">{task.author}</div>
                <div className="text-sm opacity-50">{(moment(task.created_at)).format('DD-MMM-YYYY')   }</div>
              </div>
            </div>
          </td>
          <td>
            {task.story_title}
            {/* <br/>
            <span className="badge badge-ghost badge-sm">Desktop Support Technician</span> */}
          </td>
          
          <th>
            
          <a className="btn btn-ghost btn-xs" href={task.story_url}> Detalle </a>
          <input type="checkbox" className="mask mask-star" />
          </th>
        </tr>

      ))}
     
    </tbody>
    {/* foot */}
    {/* <tfoot>
      <tr>
        <th>Name</th>
        <th>Job</th>
        <th>Favorite Color</th>
        <th></th>
      </tr>
    </tfoot>
     */}
  </table>
  <div className="join grid grid-cols-2">
  <button className="join-item btn btn-outline">Previous page</button>
  <button className="join-item btn btn-outline">Next</button>
</div>
</div>

  )
}
